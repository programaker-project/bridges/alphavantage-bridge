import logging
import os
import threading
import time
import traceback

import requests

import rate_limit

LIMITER = rate_limit.RateLimitManager()
SLEEP_TIME_BETWEEN_LOOPS = 10
DEFAULT_PRICE_KEY = "05. price"


def get_ticker_info(ticker, api_key):
    r = requests.get(
        "https://www.alphavantage.co/query",
        params={
            "function": "OVERVIEW",
            "apikey": api_key,
            "symbol": ticker,
        },
    )

    logging.info("PULL INFO '{}'".format(ticker))
    return r.json()


def get_ticker_quote(ticker, api_key):
    r = requests.get(
        "https://www.alphavantage.co/query",
        params={
            "function": "GLOBAL_QUOTE",
            "apikey": api_key,
            "symbol": ticker,
        },
    )
    logging.info("PULL QUOTE '{}'".format(ticker))
    return r.json()["Global Quote"]


class Listener(threading.Thread):
    def __init__(self, get_apikey, on_new_price):
        threading.Thread.__init__(self)
        self.tickers_listened = {}  # ticker => (last_listened, [users])
        self.cached_infos = {}
        self.cached_quotes = {}
        self.on_new_price = on_new_price
        self.get_apikey = get_apikey
        self.tickers_by_connection = {}  # Used to manage rate limit

    def listen_ticker(self, ticker_name, conn_id):
        ticker_name = str(ticker_name).upper()

        redundant = ticker_name in self.tickers_listened
        logging.info(
            "Listenening to {} {}".format(
                ticker_name, "(redundant)" if redundant else ""
            )
        )

        if ticker_name not in self.tickers_listened:
            self.tickers_listened[ticker_name] = [0, []]

        if conn_id not in self.tickers_by_connection:
            self.tickers_by_connection[conn_id] = set()

        self.tickers_by_connection[conn_id].add(ticker_name)

        if conn_id not in self.tickers_listened[ticker_name][1]:
            self.tickers_listened[ticker_name][1].append(conn_id)

        if not redundant:
            self.tickers_listened[ticker_name][0] = time.time()
            _fresh, info = self.get_price(ticker_name, conn_id)
            # Ignore freshness, as supposedly it's not redundant
            price = info[DEFAULT_PRICE_KEY]
            self.on_new_price(ticker_name, price)
            LIMITER.notify_will_use(conn_id, ticker_name)

    def get_info(self, ticker_name, conn_id):
        ticker_name = str(ticker_name).upper()

        cached = self.cached_infos.get(ticker_name, (0, None))

        # Infos are only updated at max once every day
        if time.time() - cached[0] < 86400:
            logging.info("CACHED info for '{}'".format(ticker_name))
            return (False, cached[1])

        if isinstance(cached[1], Exception):
            logging.info("CACHED exception (info) '{}'".format(ticker_name))
            raise (False, cached[1])

        try:
            info = get_ticker_info(ticker_name, self.get_apikey(conn_id))
            self.cached_infos[ticker_name] = (time.time(), info)
        except Exception as ex:
            self.cached_infos[ticker_name] = (time.time(), ex)
            raise ex
        return (True, info)

    def get_price(self, ticker_name, conn_id=None):
        ticker_name = str(ticker_name).upper()

        if conn_id is None:
            # Pull the next one that listens to it and add it to the end of the list
            conn_id = self.tickers_listened[ticker_name][1].pop()
            self.tickers_listened[ticker_name][1].insert(0, conn_id)

        cached = self.cached_quotes.get(ticker_name, (0, None))
        if cached[0] != 0 and not LIMITER.time_for_periodic_check(
            conn_id, len(self.tickers_by_connection[conn_id]), ticker_name
        ):
            logging.info("CACHED quote for '{}'".format(ticker_name))
            return (False, cached[1])

        if isinstance(cached[1], Exception):
            logging.info("CACHED exception (quote) for '{}'".format(ticker_name))
            raise (False, cached[1])

        try:
            ticker = get_ticker_quote(ticker_name, self.get_apikey(conn_id))
            self.cached_quotes[ticker_name] = (time.time(), ticker)
        except Exception as ex:
            self.cached_quotes[ticker_name] = (time.time(), ex)
            raise ex
        return (True, ticker)

    def run(self):
        while True:
            start_time = time.time()
            logging.info(
                "Checking updates. {} tickers listened".format(
                    len(self.tickers_listened.keys())
                )
            )
            for ticker in list(self.tickers_listened.keys()):
                try:
                    (fresh, info) = self.get_price(ticker)
                    if fresh:
                        price = info[DEFAULT_PRICE_KEY]
                        self.on_new_price(ticker, price)
                except:
                    logging.error(traceback.format_exc())

            time.sleep(SLEEP_TIME_BETWEEN_LOOPS)
