import logging
import time

# Information from https://www.alphavantage.co/support/#support
SECONDS = 1
MINUTES = 60 * SECONDS
HOURS = 60 * MINUTES
DAYS = 24 * HOURS

# How much "free-space" should be left for non-accounted additions.
# Later added checks can get some slack from this margin
RATE_LIMIT_MARGIN = 0.5
assert 0 < RATE_LIMIT_MARGIN < 1

MIN_UPDATE_PERIOD = 60  # Minimal time between updates on a free user account
FREE_ACCOUNT_LIMIT = 500

GLOBAL_ENDPOINT_INFO = {
    "limit_window": 1 * DAYS,
    "per_user_limit": FREE_ACCOUNT_LIMIT,
}


class RateLimitManager:
    def __init__(self):
        self.usage_info = {}

    def notify_will_use(self, connection_id, queried_element):
        if connection_id not in self.usage_info:
            self.usage_info[connection_id] = {"active": None, "check": {}}

        self.usage_info[connection_id]["check"][queried_element] = time.time()
        self.usage_info[connection_id]["active"] = time.time()

    def time_for_periodic_check(
        self, connection_id, queries_in_bucket, queried_element=None
    ):
        endpoint_info = GLOBAL_ENDPOINT_INFO

        single_check_update_period = (
            endpoint_info["limit_window"] / endpoint_info["per_user_limit"]
        ) / RATE_LIMIT_MARGIN
        per_element_update_period = single_check_update_period * queries_in_bucket

        logging.info(
            "UPDATE_PERIOD (limit={}, window={}, bucket={}): {}".format(
                endpoint_info["per_user_limit"],
                endpoint_info["limit_window"],
                queries_in_bucket,
                per_element_update_period,
            )
        )

        if connection_id not in self.usage_info:
            self.usage_info[connection_id] = {"active": None, "check": {}}

        last_time_checked = self.usage_info[connection_id]["check"].get(
            queried_element, None
        )

        time_to_update = False
        if last_time_checked is None:
            time_to_update = True
        else:
            time_since_update = time.time() - last_time_checked
            time_to_update = (time_since_update > MIN_UPDATE_PERIOD) and (
                time_since_update > per_element_update_period
            )
            if not time_to_update:
                logging.info(
                    "NOT UPDATING ({} < {}) or ({} < {}). {:.2f} before next check".format(
                        time_since_update,
                        MIN_UPDATE_PERIOD,
                        time_since_update,
                        per_element_update_period,
                        max(
                            MIN_UPDATE_PERIOD - time_since_update,
                            per_element_update_period - time_since_update,
                        ),
                    )
                )

        if time_to_update:
            logging.info("UPDATING")

        if time_to_update:
            self.usage_info[connection_id]["check"][queried_element] = time.time()

        return time_to_update
