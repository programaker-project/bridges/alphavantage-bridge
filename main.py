import logging
import os
import traceback

from programaker_bridge import (BlockArgument, BlockContext,
                                CallbackBlockArgument,
                                FormBasedServiceRegistration,
                                ProgramakerBridge, VariableBlockArgument)
from programaker_bridge.storage import Storage

import price_listener

ENDPOINT = os.environ["BRIDGE_ENDPOINT"]
AUTH_TOKEN = os.environ["BRIDGE_TOKEN"]
IS_PUBLIC = os.getenv("BRIDGE_IS_PUBLIC", "0").lower() in ("1", "t", "true")
APP_ID = "AlphaVantage"
STORAGE = Storage(APP_ID)


class Registerer(FormBasedServiceRegistration):
    def get_call_to_action_text(self, extra_data):
        return """
        <a href="https://www.alphavantage.co/support/#api-key">Get your AlphaVantage API Key here</a>:

        <input name="apikey" type="text" value="" placeholder="Your AlphaVantage API Key">
        """

    def register(self, data, extra_data):
        user_id = extra_data.user_id
        key = data["form"]["apikey"]

        # If it didn't fail, save the user data
        STORAGE.create_user(user_id, {"apikey": key})

        return True


bridge = ProgramakerBridge(
    name="Alpha Vantage",
    endpoint=ENDPOINT,
    is_public=IS_PUBLIC,
    events=[
        "on_new_price",
    ],
    token=AUTH_TOKEN,
    icon=open("logo.png", "rb"),
)

REGISTERER = Registerer(bridge=bridge)
bridge.registerer = REGISTERER

on_new_price = bridge.events.on_new_price

on_new_price.add_trigger_block(
    id="on_new_price",
    message="On new price for %1. Save to %2",
    arguments=[
        BlockArgument(str, "AAPL"),
        VariableBlockArgument(float),
    ],
    subkey=BlockContext.ARGUMENTS[0],
    save_to=BlockContext.ARGUMENTS[1],
)


def notify_new_price(item, price):
    logging.info(f"NOTIFYING {item} ⇒ {price}")
    on_new_price.send(to_user=None, content=price, event={item: price}, subkey=item)


def get_api_key(conn_id):
    with STORAGE.on_user(conn_id) as user:
        return user["apikey"]


LISTENER = price_listener.Listener(get_api_key, notify_new_price)


@on_new_price.on_new_listeners
def on_new_price_listeners(user, subkey):
    if subkey is not None and subkey != "__all__":
        LISTENER.listen_ticker(subkey.upper(), user)


@bridge.callback
def get_quote_types(_extra_data):
    return [
        {"name": "price", "id": "05. price"},
        {"name": "open price", "id": "02. open"},
        {"name": "close price", "id": "08. previous_close"},
        {"name": "high price", "id": "03. high"},
        {"name": "low price", "id": "04. low"},
        {"name": "volume", "id": "06. volume"},
        {"name": "latest trading day", "id": "07. latest_trading_day"},
        {"name": "price change", "id": "09. change"},
    ]


@bridge.callback
def get_info_types(_extra_data):
    return [
        {"name": "200 day moving average", "id": "200DayMovingAverage"},
        {"name": "50 day moving average", "id": "50DayMovingAverage"},
        {"name": "52 week high", "id": "52WeekHigh"},
        {"name": "52 week low", "id": "52WeekLow"},
        {"name": "Address", "id": "Address"},
        {"name": "Analyst target price", "id": "AnalystTargetPrice"},
        {"name": "Asset type", "id": "AssetType"},
        {"name": "Beta", "id": "Beta"},
        {"name": "Book value", "id": "BookValue"},
        {"name": "Country", "id": "Country"},
        {"name": "Currency", "id": "Currency"},
        {"name": "Description", "id": "Description"},
        {"name": "Diluted EPSTTM", "id": "DilutedEPSTTM"},
        {"name": "Dividend date", "id": "DividendDate"},
        {"name": "Dividend per share", "id": "DividendPerShare"},
        {"name": "Dividend yield", "id": "DividendYield"},
        {"name": "EBITDA", "id": "EBITDA"},
        {"name": "EPS", "id": "EPS"},
        {"name": "EV to EBITDA", "id": "EVToEBITDA"},
        {"name": "EV to Revenue", "id": "EVToRevenue"},
        {"name": "ExDividend date", "id": "ExDividendDate"},
        {"name": "Exchange", "id": "Exchange"},
        {"name": "Fiscal year end", "id": "FiscalYearEnd"},
        {"name": "Forward annual dividend rate", "id": "ForwardAnnualDividendRate"},
        {"name": "Forward annual dividend yield", "id": "ForwardAnnualDividendYield"},
        {"name": "Forward PE", "id": "ForwardPE"},
        {"name": "Full time employees", "id": "FullTimeEmployees"},
        {"name": "Gross profit TTM", "id": "GrossProfitTTM"},
        {"name": "Industry", "id": "Industry"},
        {"name": "Last split date", "id": "LastSplitDate"},
        {"name": "Last split factor", "id": "LastSplitFactor"},
        {"name": "Latest quarter", "id": "LatestQuarter"},
        {"name": "Market capitalization", "id": "MarketCapitalization"},
        {"name": "Name", "id": "Name"},
        {"name": "Operating margin TTM", "id": "OperatingMarginTTM"},
        {"name": "PEG ratio", "id": "PEGRatio"},
        {"name": "PE ratio", "id": "PERatio"},
        {"name": "Payout ratio", "id": "PayoutRatio"},
        {"name": "Percent insiders", "id": "PercentInsiders"},
        {"name": "Percent institutions", "id": "PercentInstitutions"},
        {"name": "Price to book ratio", "id": "PriceToBookRatio"},
        {"name": "Price to sales ratio TTM", "id": "PriceToSalesRatioTTM"},
        {"name": "Profit margin", "id": "ProfitMargin"},
        {"name": "Quarterly earnings growth YOY", "id": "QuarterlyEarningsGrowthYOY"},
        {"name": "Quarterly revenue growth YOY", "id": "QuarterlyRevenueGrowthYOY"},
        {"name": "Return on assets TTM", "id": "ReturnOnAssetsTTM"},
        {"name": "Return on equity TTM", "id": "ReturnOnEquityTTM"},
        {"name": "Revenue per share TTM", "id": "RevenuePerShareTTM"},
        {"name": "Revenue TTM", "id": "RevenueTTM"},
        {"name": "Sector", "id": "Sector"},
        {"name": "Shares float", "id": "SharesFloat"},
        {"name": "Shares outstanding", "id": "SharesOutstanding"},
        {"name": "Shares short", "id": "SharesShort"},
        {"name": "Shares short prior month", "id": "SharesShortPriorMonth"},
        {"name": "Short percent float", "id": "ShortPercentFloat"},
        {"name": "Short percent outstanding", "id": "ShortPercentOutstanding"},
        {"name": "Short ratio", "id": "ShortRatio"},
        {"name": "Trailing PE", "id": "TrailingPE"},
    ]


@bridge.getter(
    id="get_price_for_id",  # Give it an ID
    message="Get %1 for %2",  # Set block message
    arguments=[
        CallbackBlockArgument(str, get_quote_types),
        BlockArgument(str, "IBM"),
    ],
)
def get_price(item, ticker_name, extra_data):
    logging.info("GET PRICE '{}' @ '{}'".format(item, ticker_name))
    fresh, data = LISTENER.get_price(ticker_name, extra_data.user_id)
    logging.info(f"Fresh: {fresh}; data: {data}")
    return data[item]


@bridge.getter(
    id="get_info_for_id",  # Give it an ID
    message="Get %1 for %2",  # Set block message
    arguments=[
        CallbackBlockArgument(str, get_info_types),
        BlockArgument(str, "IBM"),
    ],
)
def get_info(item, ticker_name, extra_data):
    logging.info("GET INFO '{}' @ '{}'".format(item, ticker_name))
    fresh, data = LISTENER.get_info(ticker_name, extra_data.user_id)
    logging.info(f"Fresh: {fresh}; data: {data}")
    return data[item]


if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s - %(levelname)s [%(filename)s] %(message)s")
    logging.getLogger().setLevel(logging.INFO)

    LISTENER.start()
    try:
        bridge.run()
    except Exception:
        traceback.print_exc()
        os._exit(1)

    os._exit(0)  # Force stopping after the bridge ends
