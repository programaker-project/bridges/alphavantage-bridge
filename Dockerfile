FROM python:3-alpine

WORKDIR /usr/src/app

# Note that everything is uninstalled later.
ADD requirements.txt ./

RUN apk add --no-cache g++ libpq postgresql-dev  && \
    pip install -U -r requirements.txt && \
    apk del g++ postgresql-dev && \
    rm -v requirements.txt

COPY . ./

CMD ["python3", "main.py"]
